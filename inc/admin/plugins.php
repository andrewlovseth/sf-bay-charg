<?php
/**
 * Add required and recommended plugins.
 *
 * @package Digimag
 */

add_action( 'tgmpa_register', 'greentech_register_required_plugins' );

/**
 * Register required plugins
 *
 * @since  1.0
 */
function greentech_register_required_plugins() {
	$plugins = greentech_required_plugins();

	$config = array(
		'id'          => 'greentech',
		'has_notices' => false,
	);

	tgmpa( $plugins, $config );
}

/**
 * List of required plugins
 */
function greentech_required_plugins() {
	return array(
		array(
			'name'     => esc_html__( 'Jetpack', 'greentech' ),
			'slug'     => 'jetpack',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'One click demo import', 'greentech' ),
			'slug' => 'one-click-demo-import',
		),
		array(
			'name' => esc_html__( 'Ultimate Fonts', 'greentech' ),
			'slug' => 'ultimate-fonts',
		),
		array(
			'name' => esc_html__( 'Ultimate Colors', 'greentech' ),
			'slug' => 'ultimate-colors',
		),
	);
}
