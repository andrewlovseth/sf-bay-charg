<?php
/**
 * Get CSS for color scheme.
 *
 * @return string
 */

function greentech_customizer_custom_theme_color() {

	/* Get Option */
	$main_color = get_theme_mod( 'main_color' );
	$hover_color = get_theme_mod( 'hover_color' );

	$css = '';

	// Theme color
	if ( $main_color ) {
		$css .= sprintf( '
			a:hover, a:focus, a:active,
			.widget-footer .aside-post .name a:hover,
			.aside-post .name a:hover,
			.statistics-text,
			.site-search .site-search-toggler,
			.statistics-textarea h2,
			.tes .info .name,
			.main-navigation ul.menu .sub-menu li:hover > a,
			.site-logo .site-title a,
			.widget_recent_comments a, .widget_rss a,
			.widget_archive ul li a:hover,
			.widget_categories ul li a:hover,
			.post-type-archive-jetpack-portfolio .project-nav ul li:first-child a,
			.project-nav ul li.active a,
			.search .comments-link .icon,
			.blog .comments-link .icon,
			.archive .comments-link .icon,
			blockquote cite,
			.comments-area ol .reply a,
			.entry-footer .cat-links a,
			.topbar a:hover,
			.widget-footer ul li a:hover,
			.widget_archive ul li, .widget_categories ul li,
			td a, th a, caption a,
			.menu-toggle,
			.mobile-navigation ul.mobile-menu > li.current_page_item > a, .mobile-navigation ul.mobile-menu > li.current-menu-item > a, .mobile-navigation ul.mobile-menu > li.current_page_ancestor > a, .mobile-navigation ul.mobile-menu > li.current-menu-ancestor > a,
			.page-header span a:hover, .page-header a:hover {
				color: %1$s;
			}
		', $main_color );


		$css .= sprintf( '
			.main-navigation li:hover > a,
			.main-navigation li.focus > a,
			.featured-block:before,
			.service .image,
			.main-navigation div > ul > .current_page_item > a,
			.main-navigation div > ul > .current-menu-item > a,
			.main-navigation .current_page_ancestor > a,
			.main-navigation .current-menu-ancestor > a,
			.btn-primary,
			.scroll-to-top,
			.project .project-info::before,
			.tess .slick-dots li.slick-active button,
			button, input[type="button"],
			input[type="reset"],
			input[type="submit"],
			nav.navigation.pagination .page-numbers,
			th,
			button:hover,
			input[type="button"]:hover,
			input[type="reset"]:hover,
			input[type="submit"]:hover {
				background-color: %1$s;
			}
		', $main_color );

		$css .= sprintf( '
			.scroll-to-top,
			.tess .slick-dots li.slick-active button,
			button, input[type="button"],
			input[type="reset"],
			input[type="submit"],
			button:active,
			button:focus,
			input[type="button"]:active,
			input[type="button"]:focus,
			input[type="reset"]:active,
			input[type="reset"]:focus,
			input[type="submit"]:active,
			input[type="submit"]:focus,
			.btn,
			blockquote {
				border-color: %1$s;
			}
		', $main_color );

		$css .= sprintf( '
			.tess::after {
				border-left-color: %1$s;
			}
		', $main_color );
	}

	// Hover color
	if ( $hover_color ) {
		$css .= sprintf( '
			.site-search .site-search-toggler:hover,
			.widget_calendar table th {
				color: %1$s;
			}
		', $hover_color );

		$css .= sprintf( '
			.btn-primary:hover,
			.service:hover .image,
			.scroll-to-top:hover,
			button, input[type="button"]:hover,
			input[type="reset"]:hover,
			input[type="submit"]:hover,
			nav.navigation.pagination .page-numbers.current, nav.navigation.pagination .page-numbers:hover {
				background-color: %1$s;
			}
		', $hover_color );

		$css .= sprintf( '
			.scroll-to-top:hover,
			button, input[type="button"]:hover,
			input[type="reset"]:hover,
			input[type="submit"]:hover {
				border-color: %1$s;
			}
		', $hover_color );
	}

	return $css;
}