<?php
/**
 * Output custom CSS for the theme.
 *
 * @package Greentech
 */

/**
 * Output custom CSS of the theme in <head>.
 */
class Greentech_Custom_CSS {
	/**
	 * Add hook to <head> to output custom CSS.
	 */
	public function __construct() {
		$this->load_files();
		add_action( 'wp_enqueue_scripts', array( $this, 'output' ) );
	}

	/**
	 * Load custom files for custom CSS.
	 */
	public function load_files() {
		include_once get_template_directory() . '/inc/color/custom-theme-color.php';
	}

	/**
	 * Output custom CSS.
	 */
	public function output() {
		$css = $this->get_css();
		if ( $css ) {
			wp_add_inline_style( 'greentech-style', $css );
		}
	}

	/**
	 * Get custom CSS.
	 */
	protected function get_css() {
		$css = '';

		// Include custom colors.
		$css .= greentech_customizer_custom_theme_color();

		return $css;
	}
}
