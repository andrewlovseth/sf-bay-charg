<?php
/**
 * Tabs navigation.
 *
 * @package Greentech
 */

?>
<h2 class="nav-tab-wrapper">
	<a href="#getting-started" class="nav-tab nav-tab-active"><?php esc_html_e( 'Getting Started', 'greentech' ); ?></a>
	<a href="#actions" class="nav-tab"><?php esc_html_e( 'Actions', 'greentech' ); ?></a>
	<a href="#recommendation" class="nav-tab"><?php esc_html_e( 'Recommendation', 'greentech' ); ?></a>
</h2>
