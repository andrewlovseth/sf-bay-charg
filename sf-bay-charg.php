<?php

/*

	Template Name: SF Bay CHARG

*/

get_header(); ?>


	<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

	    <?php if( get_row_layout() == 'hero_large' ): ?>

		    <section class="hero hero-large">
				<?php if(have_rows('photos')): while(have_rows('photos')): the_row(); ?>
				 
					<?php $link = get_sub_field('link'); if( $link ): ?>

						<?php
						    $link_url = $link['url'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						?>

						<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
					<?php endif; ?>

						<article class="photo">
							<div class="content">

								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

								<div class="info">
									<div class="wrapper">
										<h1><?php the_sub_field('headline'); ?></h1>
									</div>   
								</div>							

							</div> 		
						</article>

					<?php if($link): ?>
						</a>
					<?php endif; ?>	

				<?php endwhile; endif; ?>
		    </section>



		
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'hero_small' ): ?>

			<section class="hero hero-small cover" style="background-image: url(<?php $heroPhoto = get_sub_field('photo'); echo $heroPhoto['url']; ?>);">
				<div class="content">

				</div> 		
			</section>
			
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'hero_sub_headline' ): ?>

			<section class="hero-sub-headline">
				<div class="wrapper">

					<div class="headline">
						<h2><?php the_sub_field('headline'); ?></h2>
					</div>

				</div> 		
			</section>
			
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'full_bleed' ): ?>

			<section class="full-bleed">
				<div class="wrapper">

					<div class="headline">
						<h3><?php the_sub_field('headline'); ?></h3>
					</div>

					<div class="copy">
						<?php the_sub_field('copy'); ?>
					</div>


				</div> 		
			</section>
			
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'inset_quote' ): ?>

			<section class="inset-quote">
				<div class="wrapper">

					<blockquote>
						<?php the_sub_field('quotation'); ?>
					</blockquote>

					<div class="graphic">
						<img src="<?php bloginfo('template_directory'); ?>/images/wave-small.png" alt="Swoosh">
					</div>

				</div> 		
			</section>
			
	    <?php endif; ?>
	 
	    <?php if( get_row_layout() == 'text' ): ?>
			
			<section class="text <?php the_sub_field('color_scheme'); ?> <?php the_sub_field('alignment'); ?>">
				<div class="wrapper<?php if(get_sub_field('show_image')): ?> flex<?php endif; ?>">

					<div class="info">
						<h2 class="<?php the_sub_field('headline_alignment'); ?>"><?php the_sub_field('headline'); ?></h2>
						<?php the_sub_field('copy'); ?>

						<?php if(get_sub_field('show_cta')): ?>
							<div class="cta">
								<a href="<?php the_sub_field('cta_link'); ?>"><?php the_sub_field('cta_label'); ?></a>
							</div>
						<?php endif; ?>
					</div>

					<?php if(get_sub_field('show_image')): ?>

						<div class="image <?php the_sub_field('image_align'); ?>">
							<?php if(get_sub_field('image_link')): ?>
								<a href="<?php the_sub_field('image_link'); ?>">
							<?php endif; ?>
							
							<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php if(get_sub_field('image_link')): ?>
								</a>
							<?php endif; ?>

							<?php if($image['caption']): ?>
								<div class="caption">
									<em><?php echo $image['caption']; ?></em>
								</div>
							<?php endif; ?>
						</div>

					<?php endif; ?>


				</div>    		
			</section>
			
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'inset' ): ?>
			
			<section class="inset">
				<div class="wrapper">

					<div class="inset-content">
						<h3><?php the_sub_field('headline'); ?></h3>
						<?php the_sub_field('copy'); ?>						
					</div>

				</div>    		
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'people_list' ): ?>
			
			<section class="people-list <?php the_sub_field('color_scheme'); ?>">
				<div class="wrapper">

					<div class="info">
						<h2><?php the_sub_field('headline'); ?></h2>
						<?php the_sub_field('copy'); ?>
					</div>

					<div class="people">
						<?php if(have_rows('people')): while(have_rows('people')): the_row(); ?>

							<?php if(get_sub_field('logo')): ?>

							    <div class="person has-logo">
							    	<div class="logo">
							    		<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    	</div>

							    	<div class="info">
								        <h4><?php the_sub_field('name'); ?></h4>
								        <p><?php the_sub_field('description'); ?></p>
							    	</div>
							    </div>

							<?php else: ?>

							    <div class="person">
							    	<div class="info">
								        <h4><?php the_sub_field('name'); ?></h4>
								        <p><?php the_sub_field('description'); ?></p>
							    	</div>
							    </div>

							<?php endif; ?>

						<?php endwhile; endif; ?>
					</div>

				</div>    		
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'accordion_section' ): ?>
			
			<section class="accordion<?php if(get_sub_field('inset')): ?> inset<?php endif; ?>">
				<div class="wrapper">

					<?php if(get_sub_field('headline')): ?>

						<div class="info">
							<h3><?php the_sub_field('headline'); ?></h3>
						</div>

					<?php endif; ?>

					<?php if(have_rows('accordion')): while(have_rows('accordion')): the_row(); ?>
 
				    	<?php if(get_sub_field('section_header')): ?>
					    	<div class="section-header">
					    		<h2><?php the_sub_field('section_header'); ?></h2>
					    	</div>
					    <?php endif; ?>

					    <div class="accordion-item">
					    	<div class="header">
					    		<strong><?php the_sub_field('header'); ?></strong>
					    	</div>

					    	<div class="copy">
					    		<?php the_sub_field('copy'); ?>
					    	</div>
					    </div>

					<?php endwhile; endif; ?>

				</div>    		
			</section>
			
	    <?php endif; ?>


	    <?php if( get_row_layout() == 'contact_form' ): ?>
			
			<section class="contact-form">
				<div class="wrapper">

					<div class="header">
						<h2><?php the_sub_field('headline'); ?></h2>
					</div>

					<div class="form">
						<?php echo do_shortcode(get_sub_field('shortcode')); ?>
					</div>

				</div>    		
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == '2_col_inset' ): ?>
			
			<section class="two-col-inset">
				<div class="wrapper">

					<div class="info">
						<div class="copy">
							<?php the_sub_field('copy'); ?>
						</div>
					</div>

					<div class="photo">
						<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					
				</div>
			</section>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'what_we_think_slider' ): ?>

	    	<section class="quote-slider">
	    		<div class="wrapper">

	    			<div class="header">
	    				<h3><?php the_sub_field('headline'); ?></h3>
	    				<img src="<?php bloginfo('template_directory'); ?>/images/wave-small.png" alt="Swoosh">
	    			</div>
	    		</div>

	    		<div class="quotes">
		    		<?php if(have_rows('quotes')): while(have_rows('quotes')): the_row(); ?>
					 
					    <div class="quote">
					    	<div class="mark">
					    		<span>“</span>
					    	</div>
					        
					        <div class="copy">
					        	<p><?php the_sub_field('quote'); ?></p>
					        	<h5>&mdash; <?php the_sub_field('source'); ?></h5>
					        	<h6><?php the_sub_field('source_description'); ?></h6>
					        </div>
					    </div>

					<?php endwhile; endif; ?>
	    		</div>


	    		


	    	</section>


	    <?php endif; ?>

	    <?php if( get_row_layout() == 'footer_photos' ): ?>
			
			<section class="footer-photos">
				<div class="wrapper">

					<div class="info">
						<h3>All our creeks drain to San Francisco Bay.</h3>
						<p>As sea levels rise, high water along the shore will flood our creeks and communities more often.</p>
					</div>
			
					<?php $images = get_sub_field('gallery'); if( $images ): ?>
		
						<div class="gallery">
							<?php foreach( $images as $image ): ?>
								<div class="photo">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<div class="caption">
										<p><?php echo $image['caption']; ?></p>
									</div>
								</div>
							<?php endforeach; ?>
						</div>

					<?php endif; ?>

				</div>    		
			</section>
			
	    <?php endif; ?>


	<?php endwhile; endif; ?>

<?php get_footer(); ?>